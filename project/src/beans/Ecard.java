package beans;

import java.io.Serializable;
import java.util.Random;

public class Ecard implements Serializable {

	//相手の使ったカードを記録する
	private int e1=0;
	private int e2=0;
	private int e3=0;
	private int e4=0;
	private int e5=0;
	private int e6=0;
	private int e7=0;
	private int e8=0;
	private int e9=0;
	private int e10=0;
	private int ej=0;
	//相手の選んだカード
	private int ennum=0;
	//残りカード
	private int card=11;
	//相手の勝利回数を記録する
	private int ewt=0;

	//相手の選択したカードを取得&記録&残りカードを１減らす
	public int useecard(){
		Random random=new Random();
		boolean stop = false;
		while(stop==false) {
			this.ennum = random.nextInt(11)+1;
			switch(ennum) {
				case 1:
					if(e1==0) {
						e1=1;
						card=card-1;
						stop=true;
					}
					break;
				case 2:
					if(e2==0) {
						e2=1;
						card=card-1;
						stop=true;
					}
					break;
				case 3:
					if(e3==0) {
						e3=1;
						card=card-1;
						stop=true;
					}
					break;
				case 4:
					if(e4==0) {
						e4=1;
						card=card-1;
						stop=true;
					}
					break;
				case 5:
					if(e5==0) {
						e5=1;
						card=card-1;
						stop=true;
					}
					break;
				case 6:
					if(e6==0) {
						e6=1;
						card=card-1;
						stop=true;
					}
					break;
				case 7:
					if(e7==0) {
						e7=1;
						card=card-1;
						stop=true;
					}
					break;
				case 8:
					if(e8==0) {
						e8=1;
						card=card-1;
						stop=true;
					}
					break;
				case 9:
					if(e9==0) {
						e9=1;
						card=card-1;
						stop=true;
					}
					break;
				case 10:
					if(e10==0) {
						e10=1;
						card=card-1;
						stop=true;
					}
					break;
				case 11:
					if(ej==0) {
						ej=1;
						card=card-1;
						stop=true;
					}
					break;
			}
		}
		return ennum;
	}

	//負けの場合、相手の勝利数を加算する
	public void ewin(String result) {
		if(result=="lose") {
			if(ennum==7) {
				ewt=ewt+2;
			}else {
				ewt=ewt+1;
			}
		}
	}




	public int gete1() {
		return e1;
	}
	public void sete1(int e1) {
		this.e1 = e1;
	}
	public int gete2() {
		return e2;
	}
	public void sete2(int e2) {
		this.e2 = e2;
	}
	public int gete3() {
		return e3;
	}
	public void sete3(int e3) {
		this.e3 = e3;
	}
	public int gete4() {
		return e4;
	}
	public void sete4(int e4) {
		this.e4 = e4;
	}
	public int gete5() {
		return e5;
	}
	public void sete5(int e5) {
		this.e5 = e5;
	}
	public int gete6() {
		return e6;
	}
	public void sete6(int e6) {
		this.e6 = e6;
	}
	public int gete7() {
		return e7;
	}
	public void sete7(int e7) {
		this.e7 = e7;
	}
	public int gete8() {
		return e8;
	}
	public void sete8(int e8) {
		this.e8 = e8;
	}
	public int gete9() {
		return e9;
	}
	public void sete9(int e9) {
		this.e9 = e9;
	}
	public int gete10() {
		return e10;
	}
	public void sete10(int e10) {
		this.e10 = e10;
	}
	public int getej() {
		return ej;
	}
	public void setej(int ej) {
		this.ej = ej;
	}
	public int getewt() {
		return ewt;
	}
	public void setewt(int ewt) {
		this.ewt = ewt;
	}
	public int getcard() {
		return card;
	}
	public void setcard(int card) {
		this.card = card;
	}



}