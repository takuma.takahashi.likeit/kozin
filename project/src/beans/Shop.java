package beans;

public class Shop {
	private int id;
	private int price;
	private String filename;


	// 全てのデータをセットするコンストラクタ
	public Shop(int id, int price, String filename){
		this.id = id;
		this.price = price;
		this.filename = filename;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}









}
