package beans;

import java.io.Serializable;

public class Mycard implements Serializable {

	//自分の使ったカード
	private int m1=0;
	private int m2=0;
	private int m3=0;
	private int m4=0;
	private int m5=0;
	private int m6=0;
	private int m7=0;
	private int m8=0;
	private int m9=0;
	private int m10=0;
	private int mj=0;
	//自分の勝利回数
	private int mwt=0;


	//自分の使ったカードを記録する
	public void usemycard(int mynum) {
		switch(mynum) {
			case 1:
				this.m1=1;
				break;
			case 2:
				this.m2=1;
				break;
			case 3:
				this.m3=1;
				break;
			case 4:
				this.m4=1;
				break;
			case 5:
				this.m5=1;
				break;
			case 6:
				this.m6=1;
				break;
			case 7:
				this.m7=1;
				break;
			case 8:
				this.m8=1;
				break;
			case 9:
				this.m9=1;
				break;
			case 10:
				this.m10=1;
				break;
			case 11:
				this.mj=1;
				break;
		}
	}

	//勝敗を判定し勝利回数を加算する
	public String mwin(int mynum,int ennum) {
		String result=null;
		if(mynum==1) {
			if(ennum==8 || ennum==9 || ennum==10 || ennum==5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==2) {
			if(mynum>ennum || ennum==5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==3) {
			if(mynum>ennum || ennum==11 || ennum==5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==4) {
			if(mynum>ennum || ennum==5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==5) {
			if(mynum<ennum && ennum!=11) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==6) {
			if(ennum!=11) {
				result="draw";
			}else {
				result="lose";
			}
		}
		if(mynum==7) {
			if(mynum>ennum && ennum!=5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==8) {
			if(mynum>ennum && ennum!=1 && ennum!=5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==9) {
			if(mynum>ennum && ennum!=1 && ennum!=5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==10) {
			if(mynum>ennum && ennum!=1 && ennum!=5) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==11) {
			if(ennum!=3) {
				result="win";
			}else {
				result="lose";
			}
		}
		if(mynum==ennum) {
			result="draw";
		}
		if(ennum==6) {
			if(mynum==11) {
				result="win";
			}else {
				result="draw";
			}
		}
		if(result=="win") {
			if(mynum==7) {
				mwt=mwt+2;
			}else {
				mwt=mwt+1;
			}
		}
		return result;
	}


	public int getm1() {
		return m1;
	}
	public void setm1(int m1) {
		this.m1 = m1;
	}
	public int getm2() {
		return m2;
	}
	public void setm2(int m2) {
		this.m2 = m2;
	}
	public int getm3() {
		return m3;
	}
	public void setm3(int m3) {
		this.m3 = m3;
	}
	public int getm4() {
		return m4;
	}
	public void setm4(int m4) {
		this.m4 = m4;
	}
	public int getm5() {
		return m5;
	}
	public void setm5(int m5) {
		this.m5 = m5;
	}
	public int getm6() {
		return m6;
	}
	public void setm6(int m6) {
		this.m6 = m6;
	}
	public int getm7() {
		return m7;
	}
	public void setm7(int m7) {
		this.m7 = m7;
	}
	public int getm8() {
		return m8;
	}
	public void setm8(int m8) {
		this.m8 = m8;
	}
	public int getm9() {
		return m9;
	}
	public void setm9(int m9) {
		this.m9 = m9;
	}
	public int getm10() {
		return m10;
	}
	public void setm10(int m10) {
		this.m10 = m10;
	}
	public int getmj() {
		return mj;
	}
	public void setmj(int mj) {
		this.mj = mj;
	}
	public int getmwt() {
		return mwt;
	}
	public void setmwt(int mwt) {
		this.mwt = mwt;
	}



}