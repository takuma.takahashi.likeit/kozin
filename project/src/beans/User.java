package beans;

import java.io.Serializable;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {
	private int id;
	private String name;
	private String password;
	private String address;
	private int times;
	private int win;
	private int lose;
	private int draw;
	private int money;


	// 全てのデータをセットするコンストラクタ
	public User(int id, String name, String password, String address, int times,int win, int lose, int draw, int money){
		this.id = id;
		this.name = name;
		this.password = password;
		this.address = address;
		this.times = times;
		this.win = win;
		this.lose = lose;
		this.draw = draw;
		this.money = money;
	}

	//name確認//
	public User(String name) {
		this.name = name;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	public int getWin() {
		return win;
	}
	public void setWin(int win) {
		this.win = win;
	}
	public int getlose() {
		return lose;
	}
	public void setLose(int lose) {
		this.lose = lose;
	}
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}



}
