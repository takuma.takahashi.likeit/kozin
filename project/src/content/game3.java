package content;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Ecard;
import beans.Mycard;
import beans.User;
import dao.SeisekiDao;
import dao.UserDao;

/**
 * Servlet implementation class game3
 */
@WebServlet("/game3")
public class game3 extends HttpServlet {
	private static final long serialVersionUID = 1L;


		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッション
		HttpSession session = request.getSession();

		//選択した数字をint型で取得
		int mynum = Integer.parseInt(request.getParameter("mynum"));

		//セッションから自分の情報と相手の情報を取得
		Mycard mc = (Mycard) session.getAttribute("mc");
		Ecard ec = (Ecard) session.getAttribute("ec");

		//自分の選んだカードを記録する（２度と選べなくする）
		mc.usemycard(mynum);

		//相手の選んだカードを取得＆記録&残りカードを１減らす
		int ennum = ec.useecard();

		//自分の勝利数を追加
		String result=mc.mwin(mynum,ennum);

		//相手の勝利数を追加
		ec.ewin(result);

		//自分と相手の選んだカードをリクエストパラメータにセット
		request.setAttribute("mynum", mynum);
		request.setAttribute("ennum", ennum);

		//勝敗結果をリクエストパラメータにセット
		if(result=="win") {
			request.setAttribute("win", "勝ち");
		}
		if(result=="lose") {
			request.setAttribute("lose", "負け");
		}
		if(result=="draw") {
			request.setAttribute("draw", "引き分け");
		}


		//勝敗を成績に反映
		if(ec.getcard()==0) {
			//ID情報取得
			User user = (User) session.getAttribute("userInfo");
			int id = user.getId();
			//自分の勝敗を成績に反映
			SeisekiDao seisekiDao = new SeisekiDao();
			seisekiDao.updatetimes(id);

			if(mc.getmwt()>ec.getewt()) {
				seisekiDao.updatewin(id);
				//お金を追加
				int moneyplus=(mc.getmwt()-ec.getewt())*10;
				int money=50+moneyplus;
				seisekiDao.moneyplus(id,money);
				request.setAttribute("money", money);
			}
			if(mc.getmwt()==ec.getewt()) {
				seisekiDao.updatedraw(id);
				int money=30;
				seisekiDao.moneyplus(id,money);
				request.setAttribute("money", money);
			}
			if(mc.getmwt()<ec.getewt()) {
				seisekiDao.updatelose(id);
			}
			//相手の勝敗を成績に反映
			//情報を再設定
			session.removeAttribute("userInfo");
			UserDao userDao = new UserDao();
			user = userDao.login(id);
			session.setAttribute("userInfo", user);
		}


		//ゲーム画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/game.jsp");
		dispatcher.forward(request, response);

		}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
