package content;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Ecard;
import beans.Mycard;

/**
 * Servlet implementation class game
 */
@WebServlet("/game2")
public class game2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public game2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		//セッションにユーザ情報が無い場合ログイン画面にリダイレクト

		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("login");
		}else {

			//カードセッションがある場合カードセッションを破棄
			if(session.getAttribute("mc")!=null) {
				session.removeAttribute("mc");
			}if(session.getAttribute("ec")!=null) {
				session.removeAttribute("ec");
			}

		//カードセッションを生成
		Mycard mc =new Mycard();
		Ecard ec = new Ecard();
		session.setAttribute("mc", mc);
		session.setAttribute("ec", ec);

		//ゲーム画面にフォワード
		request.setAttribute("draw", "勝敗結果");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/game.jsp");
		dispatcher.forward(request, response);

		}
	}

}
