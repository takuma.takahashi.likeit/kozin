package content;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class userupdate
 */
@WebServlet("/userupdate")
public class userupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userupdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッションにユーザ情報が無い場合ログイン画面にリダイレクト
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("login");
		}else {

		// ユーザ情報変更画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		String name = request.getParameter("name");
		String pw = request.getParameter("pw");
		String pw2 = request.getParameter("pw2");
		String address = request.getParameter("address");
		String address2 = request.getParameter("address2");
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User username=userDao.name(name);

		if(username!=null) {
			request.setAttribute("errMsg", "このユーザ名は既に使用されています");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);

		}if( !(pw.equals(pw2)) || !(address.equals(address2))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("name", name);
			request.setAttribute("address", address);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;

		}else{
			User user = (User) session.getAttribute("userInfo");
			if(name!="") {
				userDao.updatename(name,id);
				user.setName(name);
			}if(pw!="") {
				userDao.updatepw(pw,id);
			}if(address!="") {
				userDao.updateaddress(address,id);
			}
			//ホーム画面へ
			response.sendRedirect("home");
		}

	}

}
