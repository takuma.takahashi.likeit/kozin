package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.User;



public class UserDao {

	//ログイン（ユーザ名とパスワードから検索）//
	public User login(String name, String pw){
		Connection con=null;
		try {
			con = DBManager.getConnection();

			String sql= "SELECT * FROM user WHERE name = ? and password = MD5(?)";

			PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, pw);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            int idData = rs.getInt("id");
            String nameData = rs.getString("name");
            String passwordDate = rs.getString("password");
            String addressData = rs.getString("address");
            int timesData = rs.getInt("times");
            int winData = rs.getInt("win");
            int loseData = rs.getInt("lose");
            int drawData = rs.getInt("draw");
            int moneyData = rs.getInt("money");

            return new User(idData, nameData, passwordDate, addressData, timesData, winData, loseData, drawData,moneyData);


		} catch (SQLException e) {
            e.printStackTrace();
            return null;

		} finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}
	//IDからユーザ情報を取得
	public User login(String id){
		Connection con=null;
		try {
			con = DBManager.getConnection();

			String sql= "SELECT * FROM user WHERE id = ? ";

			PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setString(1, id);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            int idData = rs.getInt("id");
            String nameData = rs.getString("name");
            String passwordDate = rs.getString("password");
            String addressData = rs.getString("address");
            int timesData = rs.getInt("times");
            int winData = rs.getInt("win");
            int loseData = rs.getInt("lose");
            int drawData = rs.getInt("draw");
            int moneyData = rs.getInt("money");

            return new User(idData, nameData, passwordDate, addressData, timesData, winData, loseData, drawData,moneyData);


		} catch (SQLException e) {
            e.printStackTrace();
            return null;

		} finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//IDからユーザ情報を取得(int)
		public User login(int id){
			Connection con=null;
			try {
				con = DBManager.getConnection();

				String sql= "SELECT * FROM user WHERE id = ? ";

				PreparedStatement pStmt = con.prepareStatement(sql);
	            pStmt.setInt(1, id);
	            ResultSet rs = pStmt.executeQuery();

	            if (!rs.next()) {
	                return null;
	            }

	            int idData = rs.getInt("id");
	            String nameData = rs.getString("name");
	            String passwordDate = rs.getString("password");
	            String addressData = rs.getString("address");
	            int timesData = rs.getInt("times");
	            int winData = rs.getInt("win");
	            int loseData = rs.getInt("lose");
	            int drawData = rs.getInt("draw");
	            int moneyData = rs.getInt("money");

	            return new User(idData, nameData, passwordDate, addressData, timesData, winData, loseData, drawData,moneyData);


			} catch (SQLException e) {
	            e.printStackTrace();
	            return null;

			} finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
		}

	//name確認//
	public User name(String name) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE name = ? ";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String nameData = rs.getString("name");
            return new User(nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	//新規登録
	public void newuser(String name,String pw,String address) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String insertSQL = "INSERT INTO user VALUES(id,?,MD5(?),?,0,0,0,0,0)";

            stmt = conn.prepareStatement(insertSQL);
            stmt.setString(1, name);
            stmt.setString(2, pw);
            stmt.setString(3, address);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ユーザ情報変更
	//name
	public void updatename(String name,String id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET name = ? WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setString(1, name);
            stmt.setString(2, id);
            stmt.executeUpdate();

        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//pw
	public void updatepw(String pw,String id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET password = MD5(?) WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setString(1, pw);
            stmt.setString(2, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//address
	public void updateaddress(String address,String id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET address = ? WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setString(1, address);
            stmt.setString(2, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//削除
	 public void delete(String id) {
	        Connection conn = null;
	        PreparedStatement stmt = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "DELETE FROM user WHERE login_id = ? ";

	             // SELECTを実行し、結果表を取得
	            stmt = conn.prepareStatement(sql);
	            stmt.setString(1, id);
	            stmt.executeUpdate();

	        } catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (stmt != null) {
						stmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

}
