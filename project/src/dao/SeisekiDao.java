package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class SeisekiDao {

	//戦闘回数を加算する
	public void updatetimes(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET times = times+1 WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//勝利数を加算する
	public void updatewin(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET win = win+1 WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//敗北数を加算する
	public void updatelose(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET lose = lose+1 WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//引き分け数を加算する
	public void updatedraw(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET draw = draw+1 WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//戦闘結果からお金を増やす
	public void moneyplus(int id,int money) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String SQL = "UPDATE user SET money = money+? WHERE id= ? ";

            stmt = conn.prepareStatement(SQL);
            stmt.setInt(1, money);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
