<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="css/game.css">
		<title>ゲーム画面</title>
	</head>
	<body>
		<p class="aida"><br></p>

		<p>
			<!-- 相手の残りカードを出力（裏面） -->
			<c:forEach begin="1" end="${ec.card}">
				<img class="size" src="image/uramen.png">&nbsp;
			</c:forEach>
			<!-- 全部カードが選び終わったら最終的な勝敗出力 -->
			<c:if test="${ec.card == 0}">
				<span class="mozi2"><c:if test="${mc.mwt > ec.ewt}">あなたの勝ちです</c:if></span>
				<span class="mozi1"><c:if test="${mc.mwt < ec.ewt}">あなたの負けです</c:if></span>
				<span class="mozi3"><c:if test="${mc.mwt == ec.ewt}">引き分けです</c:if></span>
			</c:if>
		</p>

		<p class="aida"><br></p>

		<div class="center">
			<!-- 勝敗を出力 -->
			<div class="sam1">
				<span class="mozi2">${win}</span>
				<span class="mozi1">${lose}</span>
				<span class="mozi3">${draw}</span>
			</div>
			<!-- 自分の選んだカードを出力 -->
			<c:if test="${mynum == null}"><img class="size" src="image/mycard.png"></c:if>
			<c:if test="${mynum == 1}"><img class="size" src="image/m1.png"></c:if>
			<c:if test="${mynum == 2}"><img class="size" src="image/m2.png"></c:if>
			<c:if test="${mynum == 3}"><img class="size" src="image/m3.png"></c:if>
			<c:if test="${mynum == 4}"><img class="size" src="image/m4.png"></c:if>
			<c:if test="${mynum == 5}"><img class="size" src="image/m5.png"></c:if>
			<c:if test="${mynum == 6}"><img class="size" src="image/m6.png"></c:if>
			<c:if test="${mynum == 7}"><img class="size" src="image/m7.png"></c:if>
			<c:if test="${mynum == 8}"><img class="size" src="image/m8.png"></c:if>
			<c:if test="${mynum == 9}"><img class="size" src="image/m9.png"></c:if>
			<c:if test="${mynum == 10}"><img class="size" src="image/m10.png"></c:if>
			<c:if test="${mynum == 11}"><img class="size" src="image/mj.png"></c:if>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			<!-- 相手の選んだカードを出力 -->
			<c:if test="${ennum == null}"><img class="size" src="image/ecard.png"></c:if>
			<c:if test="${ennum == 1}"><img class="size" src="image/e1.png"></c:if>
			<c:if test="${ennum == 2}"><img class="size" src="image/e2.png"></c:if>
			<c:if test="${ennum == 3}"><img class="size" src="image/e3.png"></c:if>
			<c:if test="${ennum == 4}"><img class="size" src="image/e4.png"></c:if>
			<c:if test="${ennum == 5}"><img class="size" src="image/e5.png"></c:if>
			<c:if test="${ennum == 6}"><img class="size" src="image/e6.png"></c:if>
			<c:if test="${ennum == 7}"><img class="size" src="image/e7.png"></c:if>
			<c:if test="${ennum == 8}"><img class="size" src="image/e8.png"></c:if>
			<c:if test="${ennum == 9}"><img class="size" src="image/e9.png"></c:if>
			<c:if test="${ennum == 10}"><img class="size" src="image/e10.png"></c:if>
			<c:if test="${ennum == 11}"><img class="size" src="image/ej.png"></c:if>
			<!-- お互いの勝ち数を出力 -->
			<div class="sam1">
				<span class="mozi2">自分の勝利数${mc.mwt}</span><br><br>
				<span class="mozi1">相手の勝利数${ec.ewt}</span>
			</div>

		</div>

		<p class="aida"><br></p>

		<!-- 自分の残りカードを出力 -->
		<p>
			<form action="game" method="post">
				<c:if test="${mc.m1 == 0}"><a href="game3?mynum=1"><img class="size" src="image/m1.png">&nbsp;</a></c:if>
				<c:if test="${mc.m2 == 0}"><a href="game3?mynum=2"><img class="size" src="image/m2.png">&nbsp;</a></c:if>
				<c:if test="${mc.m3 == 0}"><a href="game3?mynum=3"><img class="size" src="image/m3.png">&nbsp;</a></c:if>
				<c:if test="${mc.m4 == 0}"><a href="game3?mynum=4"><img class="size" src="image/m4.png">&nbsp;</a></c:if>
				<c:if test="${mc.m5 == 0}"><a href="game3?mynum=5"><img class="size" src="image/m5.png">&nbsp;</a></c:if>
				<c:if test="${mc.m6 == 0}"><a href="game3?mynum=6"><img class="size" src="image/m6.png">&nbsp;</a></c:if>
				<c:if test="${mc.m7 == 0}"><a href="game3?mynum=7"><img class="size" src="image/m7.png">&nbsp;</a></c:if>
				<c:if test="${mc.m8 == 0}"><a href="game3?mynum=8"><img class="size" src="image/m8.png">&nbsp;</a></c:if>
				<c:if test="${mc.m9 == 0}"><a href="game3?mynum=9"><img class="size" src="image/m9.png">&nbsp;</a></c:if>
				<c:if test="${mc.m10 == 0}"><a href="game3?mynum=10"><img class="size" src="image/m10.png">&nbsp;</a></c:if>
				<c:if test="${mc.mj == 0}"><a href="game3?mynum=11"><img class="size" src="image/mj.png">&nbsp;</a></c:if>
			</form>
			<c:if test="${ec.card == 0}">
				<c:if test="${money != null}">
					<span class="money">${money}円増えました<br></span>
				</c:if>
				<a href=home><input class="home1" type="submit" value="ホームへ"></a>
			</c:if>
		<p>

		<p class="aida"><br></p>






	</body>
</html>