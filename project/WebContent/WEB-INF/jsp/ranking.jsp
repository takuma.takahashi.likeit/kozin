<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="css/login.css">
		<title>ランキング画面</title>
	</head>
	<body>
		<p class="aida"><br></p>
	<!-- タイトル -->
		<p><span class="ranking">　ランキング　</span></p>
		<p class="aida"><br></p>
	<!-- ランキング表 -->
		<table class="rankinglist" border="1">
			<tr>
				<th>ユーザ名</th><th>勝利数</th><th>敗北数</th><th>引き分け数</th><th>戦闘回数</th>
			</tr>
			<c:forEach var="user" items="${winranking}" >
			<tr>
				<td>${user.name}</td>
				<td>${user.win}</td>
				<td>${user.lose}</td>
				<td>${user.draw}</td>
				<td>${user.times}</td>
			</tr>
			</c:forEach>

		</table>


	<!-- ホームへ -->
		<p class="aida"><br></p>
			<a href=home>  <input class="home2" type="submit" value="ホームへ"></a>
		<p class="aida"><br></p>


	</body>
</html>