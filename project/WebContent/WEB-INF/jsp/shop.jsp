<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="css/login.css">
		<title>ショップ画面</title>
	</head>
	<body>
		<p class="aida"><br></p>
	<!-- ユーザ名を出力 -->
		<p><span class="login">　ショップ　</span></p>
		<p class="aida"><br></p>
	<!-- 画像一覧 -->
	<div class="aaa">
		<c:forEach var="shop" items="${shop}" >
			<div class="card" style="width: 33rem;">
			  <img src="background/${shop.filename}" class="card-img-top" >
			  <div class="card-body">
			    <h5 class="card-title">$ ${shop.price}</h5>
			    <a href="#" class="btn btn-primary" style="width: 3cm; height:1.3cm;font-size:130%;">購入</a>
			  </div>
			</div>
		</c:forEach>
	</div>
	<p class="aida"><br></p>
	<!-- ホームボタンを出力 -->
		<a href=home>  <input class="home1" type="submit" value="ホームへ"></a>
	<p class="aida2"><br></p>
	</body>
</html>