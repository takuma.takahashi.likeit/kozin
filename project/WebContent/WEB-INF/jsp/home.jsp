<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="css/login.css">
		<title>ログイン画面</title>
	</head>
	<body>
		<p class="aida"><br></p>
	<!-- ユーザ名を出力 -->
		<p><span class="login">${userInfo.name}</span><span class="sonota">さん</span></p>
	<!-- 戦績を出力 -->
		<p><span class="suuzi">${userInfo.times}</span><span class="sonota2">戦</span>
		<span class="suuzi">${userInfo.win}</span><span class="sonota2">勝</span>
		<span class="suuzi">${userInfo.lose}</span><span class="sonota2">敗</span>
		<span class="suuzi">${userInfo.draw}</span><span class="sonota2">分</span></p>
		<p><span class="doru">$</span><span class="doru2">${userInfo.money}</span></p>
		<p class="aida"><br></p>
	<!-- 各ボタンを出力 -->
			<a href=game2>  <input class="home1" type="submit" value="スタート"></a>
			<p class="aida2"><br></p>
			<a href=rule>  <input class="home1" type="submit" value="ルール"></a>
			<p class="aida2"><br></p>
			<a href=shop>  <input class="home1" type="submit" value="ショップ"></a>
			<p class="aida2"><br></p>

			<span class="mozi1">
				<a href=userupdate><input class="home2" type="submit" value="登録変更"></a>
				<a href=ranking>  <input class="home2" type="submit" value="ランキング"></a>
				<a href=logout>  <input class="home2" type="submit" value="ログアウト"></a>
			</span>
			<p class="aida"><br></p>
	</body>
</html>