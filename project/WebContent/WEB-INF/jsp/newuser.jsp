<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="css/login.css">
		<title>新規登録画面</title>
	</head>
	<body>
		<p class="aida"><br></p>
		<p class="shinki">新規登録</p>
		<p class="aida"><br></p>

		<c:if test="${errMsg != null}" >
			<div class="akazi">
		  		${errMsg}
			</div>
		</c:if>
		<p class="aida2"><br></p>
		<form action="newuser" method="post">
			<input class="input" type="text" placeholder="ユーザ名" name="name" size="30">
			<p class="aida3"><br></p>
			<input class="input" type="password" placeholder="パスワード" name="pw" size="30">
			<p class="aida3"><br></p>
			<input class="input" type="password" placeholder="パスワード（確認）" name="pw2" size="30">
			<p class="aida3"><br></p>
			<input class="input" type="password" placeholder="メールアドレス" name="address" size="30">
			<p class="aida3"><br></p>
			<input class="input" type="password" placeholder="メールアドレス(確認)" name="address2" size="30">
			<p class="aida3"><br></p>
			<input class="submit" type="submit" value="新規登録">
		</form>
		<p class="aida"><br></p>



	</body>
</html>